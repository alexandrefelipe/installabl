#!/bin/bash
# cd /tmp/
clear
echo -e "Which IDE do you want to install?"
echo -e "\\n1 - Eclipse
2 - Spring Tool Suite
3 - IntelliJ IDEA Community
4 - PyCharm Community
5 - Intellij IDEA Ultimate
6 - Postman
7 - Rider
8 - Android Studio
9 - Firefox Developer Edition

Choose: "

    read INPUT
    case $INPUT in
        1)
            ( exec "./eclipse/install.sh" )
            ;;
        2)
            ( exec "./spring-tool-suite/install.sh" )
            ;;
        3)
            ( exec "./intellij-idea-community/install.sh" )
            ;;
        4)
            ( exec "./pycharm-community/install.sh" )
            ;;
        5)
            ( exec "./intellij-idea-ultimate/install.sh" )
            ;;
        6)
            ( exec "./postman/install.sh" )
            ;;
        7)
            ( exec "./rider/install.sh" )
            ;;
        8)
            ( exec "./android-studio/install.sh")
            ;;
        9)
            ( exec "./firefox-developer-edition/install.sh")
            ;;

    esac
