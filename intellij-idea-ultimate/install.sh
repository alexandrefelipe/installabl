#!/bin/bash

wget -c https://download.jetbrains.com/idea/ideaIU-2022.3.1.tar.gz -P /tmp/

cd /opt && sudo tar -xvzf /tmp/ideaIU-2022.3.1.tar.gz

mkdir /tmp/Intellij-desktop-file && cd /tmp/Intellij-desktop-file

sudo cat >'intellij-idea-ultimate-2022-3.desktop' <<EOT 
[Desktop Entry]
Name=IntelliJ IDEA 2022.3
Type=Application
Exec=/opt/idea-IU-223.8214.52/bin/idea.sh
Terminal=false
Icon=/opt/idea-IU-223.8214.52/bin/idea.svg
Comment=Enjoy Productive Java
NoDisplay=false
Categories=Development;IDE;
Name[en]=IntelliJ IDEA 2022.3
Name[en_US]=IntelliJ IDEA 2022.3
StartupWMClass=jetbrains-idea
EOT

sudo desktop-file-install /tmp/Intellij-desktop-file/intellij-idea-ultimate-2022-3.desktop

clear && echo -e "IntelliJ IDEA Ultimate installed!"
