#!/bin/bash

cd /tmp && wget -O firefox.tar.bz2 --no-check-certificate --no-proxy https://download.mozilla.org/\?product\=firefox-devedition-latest-ssl\&os\=linux64

cd /opt && sudo tar -xvjf /tmp/firefox.tar.bz2

mkdir /tmp/firefox

cd /tmp/firefox

sudo cat >'firefox.desktop' <<EOT 
[Desktop Entry]
Name=Firefox Developer Edition
Type=Application
Exec=/opt/firefox/firefox
Terminal=false
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Comment=
NoDisplay=false
Categories=Development;
Name[en]=Firefox Developer Edition
Name[en_US]=Firefox Developer Edition
StartupWMClass=firefox-aurora
EOT

sudo desktop-file-install /tmp/firefox/firefox.desktop

clear && echo -e "firefox installed!"